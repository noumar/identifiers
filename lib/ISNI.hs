{-# LANGUAGE Safe #-}
-- {-# OPTIONS_GHC -Werror #-}

-- | ISNI
--
-- International Standard Name Identifier (ISNI) as defined in ISO 27729:2012.
--
-- This module provides a datatype representation and functions for parsing and printing.
--
-- https://en.wikipedia.org/wiki/International_Standard_Name_Identifier
module ISNI
  ( ISNI,
    ISNIError,
    parse,
    toString,
  )
where

import Data.Bits
import Data.Char
import Data.List (foldl')
import Data.Word
import Prelude hiding (error)

-- | ISNI datatype
newtype ISNI = ISNI Word64
  deriving (Show)

-- | ISNIError datatype
data ISNIError = LengthError | ChecksumError | ContentError
  deriving (Show)

-- | Parse ISNI from String
--
-- >>> parse "000000012146438X"
-- Right (ISNI 4853220234)
-- >>> parse "0000000114559647"
-- Right (ISNI 4636120647)
-- >>> parse "1234567899999799"
-- Left ChecksumError
-- >>> parse "123"
-- Left LengthError
-- >>> parse "123456789A999799"
-- Left ContentError
parse :: String -> Either ISNIError ISNI
parse s
  | length s /= 16 = Left LengthError
  | otherwise = do
    ds <- mapM digits ident
    c <- chars chksum
    if validate ds c
      then Right $ ISNI $ pack $ ds <> [c]
      else Left ChecksumError
  where
    chars :: Char -> Either ISNIError Word8
    chars 'X' = Right 10
    chars d = digits d
    digits :: Char -> Either ISNIError Word8
    digits '0' = Right 0
    digits '1' = Right 1
    digits '2' = Right 2
    digits '3' = Right 3
    digits '4' = Right 4
    digits '5' = Right 5
    digits '6' = Right 6
    digits '7' = Right 7
    digits '8' = Right 8
    digits '9' = Right 9
    digits _ = Left ContentError
    chksum = last s
    ident = init s

-- | Validate identifier + checksum
validate :: [Word8] -> Word8 -> Bool
validate v c = calculate v == c

-- | Calculate checksum
--
-- Uses MOD-11-2 algorithm as defined in ISO/IEC 7064:2003
--
-- >>> calculate [0,0,0,0,0,0,0,1,2,1,4,6,4,3,8]
-- 10
-- >>> calculate [0,0,0,0,0,0,0,1,1,4,5,5,9,6,4]
-- 7
calculate :: [Word8] -> Word8
calculate = (12 -) . foldl' calc 0
  where
    calc l r = ((l + r) * 2) `mod` 11

-- | Pack to internal represention
pack :: [Word8] -> Word64
pack = foldl' (\a v -> shiftL a 4 .|. fromIntegral v) 0

-- | Unpack from internal representation
unpack :: Word64 -> [Word8]
unpack w = foldl' (\a v -> fromIntegral (shiftR w v .&. 0x0F) : a) [] [0, 4 .. 60]

-- | Convert ISNI to String
toString :: ISNI -> String
toString (ISNI w) = map chars $ unpack w
  where
    chars 10 = 'X'
    chars c = intToDigit $ fromEnum c
