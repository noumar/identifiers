{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE Safe #-}
{-# LANGUAGE ViewPatterns #-}

-- | FPIC
--
-- Finnish Personal Identity Code
--
-- This module provides a datatype representation and functions for parsing and printing.
--
-- https://dvv.fi/en/personal-identity-code
module FPIC
  ( FPIC,
    parse,
    isFemale,
    isMale,
    toText,
    prop_FPIC,
  )
where

import Control.Monad (unless)
import Data.List (elemIndex)
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Read as TR
import Data.Word
import GHC.TypeLits
import Relude (maybeToRight)
import Test.QuickCheck

-- | FPIC datatype
newtype FPIC = FPIC {fpic :: Word32}
  deriving (Show, Eq)

-- | FPICError datatype
data FPICError = LengthError | ChecksumError | ContentError | MonthError | DayError | YearError
  deriving (Show, Eq)

prop_FPIC :: FPIC -> Bool
prop_FPIC a = case parse $ toText a of
  Right b -> a == b
  Left _ -> False

instance Arbitrary FPIC where
  arbitrary = do
    sign <- chooseEnum (0, 2)
    year <- chooseEnum (0, 99)
    month <- chooseEnum (1, 12)
    day <- chooseEnum (1, 31) `suchThat` \d -> validDay d month $ isLeapYear $ year4d year sign
    end <- chooseEnum (2, 899)
    return $ FPIC (day * 100_000_000 + month * 1_000_000 + year * 10_000 + end * 10 + sign)

isFemale :: FPIC -> Bool
isFemale = even . (`rem` 1000) . (`quot` 10) . fpic

isMale :: FPIC -> Bool
isMale = not . isFemale

toText :: FPIC -> Text
toText (FPIC fp) =
  let (ss, sign) = fp `quotRem` 10
      (date, end) = ss `quotRem` 1_000
   in T.justifyRight 6 '0' (T.pack $ show date)
        <> T.singleton (sign2char sign)
        <> T.justifyRight 3 '0' (T.pack $ show end)
        <> T.singleton (controlChars !! fromEnum (ss `rem` 31))
  where
    sign2char 0 = '+'
    sign2char 1 = '-'
    sign2char 2 = 'A'

controlChars :: [Char]
controlChars = "0123456789ABCDEFHJKLMNPRSTUVWXY"

-- https://en.wikipedia.org/wiki/Leap_year#Algorithm
isLeapYear :: Integral a => a -> Bool
isLeapYear y
  | y `mod` 4 /= 0 = False
  | y `mod` 100 /= 0 = True
  | y `mod` 400 /= 0 = False
  | otherwise = True

validDay d m ly
  | m == 4 || m == 6 || m == 9 || m == 11 = (d >= 1) && (d <= 30)
  | m == 2 = (d >= 1) && (d <= 28)
  | m == 2 && ly = (d >= 1) && (d <= 29)
  | otherwise = (d >= 1) && (d <= 31)

year4d y s = y + 1800 + s * 100

-- >>> parse "131052-308T"
-- Right (FPIC {fpic = 1310523081})
parse :: Text -> Either FPICError FPIC
parse ts
  | T.length ts /= 11 = Left LengthError
  | otherwise = do
    (date, ds) <- map' $ TR.decimal $ T.init ts
    (sign, ss) <- validSign ds
    (end, es) <- valid validEnd $ TR.decimal ss
    unless (es == mempty) (Left ContentError)
    control <- toEnum <$> maybeToRight ChecksumError (elemIndex (T.last ts) controlChars)
    let long = date * 1000 + end
        (ys, year) = date `quotRem` 100
        (day, month) = ys `quotRem` 100
        ly = isLeapYear $ year4d year sign
    unless (validMonth month) (Left MonthError)
    unless (validYear year) (Left YearError)
    unless (validDay day month ly) (Left DayError)
    if long `rem` 31 == control
      then Right $ FPIC $ long * 10 + sign
      else Left ChecksumError
  where
    map' :: Either String a -> Either FPICError a
    map' (Left _) = Left ContentError
    map' (Right b) = Right b

    valid _ (Left _) = Left ContentError
    valid f (Right (a, b)) = if f a then Right (a, b) else Left ContentError

    validMonth m = (m >= 1) && (m <= 12)

    validYear y = (y >= 0) && (y <= 99)

    validEnd e = (e >= 2) && (e <= 899)

    validSign (T.uncons -> Just (c, cs))
      | c == '+' = Right (0, cs)
      | c == '-' = Right (1, cs)
      | c == 'A' = Right (2, cs)
    validSign _ = Left ContentError
